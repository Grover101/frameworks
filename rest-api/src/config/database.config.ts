import { Sequelize } from "sequelize";

// const db = new Sequelize('app', '', '', {
// 	storage: './database.sqlite',
// 	dialect: 'sqlite',
// 	logging: false,
// });

const db = new Sequelize("sequelize_ts_db", "root", "", {
  //   storage: './database.sqlite',
  host: "localhost",
  dialect: "mysql",
  logging: false,
});

// const db = new Sequelize("mysql://root:@localhost:3306/sequelize_ts_db");

export default db;

import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'

import User from '../../Models/User'

export default class UsersController {
  /**
   * index
   */
  // public async index(ctx: HttpContextContract) {
  public async index() {
    return User.all()
  }

  /**
   * store
   */
  public async store({ request, response }: HttpContextContract) {
    // var validator
    try {
      // const validation = await validator.validate({
      //   schema: schema.create({
      //     username: schema.string(),
      //     email: schema.string({}, [rules.email()]),
      //     password: schema.string({}, [rules.confirmed('password')]),
      //   }),
      //   messages: {
      //     required: 'El {{ field }} es requrido para crear una nueva cuenta',
      //     email: 'El {{ field }} no es correcto',
      //   },
      //   data: request.body(),
      // })

      const validation = await request.validate({
        schema: schema.create({
          username: schema.string({}, [rules.unique({ table: 'users', column: 'username' })]),
          email: schema.string({}, [
            rules.email(),
            rules.unique({ table: 'users', column: 'email' }),
          ]),
          password: schema.string({}, [rules.confirmed('password')]),
        }),
        messages: {
          'required': 'El {{ field }} es requrido para crear una nueva cuenta',
          'email': 'El {{ field }} no es correcto',
          'username.unique': '{{ field }} ya existe',
          'email.unique': '{{ field }} ya existe',
        },
      })

      // console.log(validator)
      // console.log(request.body())

      // const user = await User.create(request.body())
      const user = await User.create(validation)
      response.status(201)
      return user
    } catch (error) {
      // if (error.sql) {
      // delete error.sql
      // response.status(500)
      // return { error: error.sqlMessage }
      // } else {
      response.status(400)
      return error
      // }
    }

    // const user = await User.create(validator)

    // if (user.$isPersisted) {
    //   response.status(201)
    //   return user
    // } else {
    //   response.status(400)
    //   return { errors: 'Error creating user' }
    // }
  }

  /**
   * show
   */
  public async show({ params }: HttpContextContract) {
    return await User.findOrFail(params.id)
  }

  /**
   * update
   */
  public async update({ params, request, response }: HttpContextContract) {
    try {
      // console.log(request)

      // await validator.validate({
      //   schema: schema.create({
      //     id: schema.string({}, [rules.exists({ table: 'users', column: 'id' })]),
      //   }),
      //   messages: {
      //     exists: 'El {{field}} no existe',
      //   },
      //   data: params,
      // })

      // const validation = await request.validate({
      await request.validate({
        schema: schema.create({
          // id: schema.string({}, [rules.exists({ table: 'users', column: 'id' })]),
          username: schema.string(),
          email: schema.string({}, [rules.email()]),
          password: schema.string({}, [rules.confirmed('password')]),
        }),
        messages: {
          required: 'El {{ field }} es requrido para actulizar los datos',
          email: 'El {{ field }} no es correcto',
        },
      })
    } catch (error) {
      response.status(400)
      return error
    }

    const user = await User.findOrFail(params.id)

    // console.log(user)

    const body = request.body()

    user.username = body.username
    user.email = body.email
    user.password = body.password

    response.status(204)
    return user.save()
  }

  /**
   * destroy
   */
  public async destroy({ params }: HttpContextContract) {
    const user = await User.findOrFail(params.id)
    return user.delete()
    // const user = await User.find(params.id)
    // console.log(!user)
    // if (user) {
    //   response.status(204)
    //   return user.delete()
    // } else {
    //   response.status(400)
    //   return { error: 'No existe el ID' }
    // }
  }

  /**
   * login
   */
  public async login({ request, response, auth }: HttpContextContract) {
    const body = request.body()
    console.log({ auth })

    const user = await User.query().where('email', body.email).firstOrFail()

    // if (!(await Hash.verify(user.password, body.password))) {
    //   return response.badRequest('Credencial invalida')
    // }

    const token = await auth.use('api').generate(user)
    response.status(200)
    return token
  }

  /**
   * prueba
   */
  public async prueba({ auth }: HttpContextContract) {
    try {
      await auth.use('api').authenticate()
      // if (auth.use('api').isAuthenticated) {
      return User.all()
      // }
    } catch (error) {
      return error
    }
  }
}
